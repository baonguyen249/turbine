import React, {Fragment, PureComponent} from "react";
import {Animated, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import _ from "lodash"
import memoize from "memoize-one";

interface Props {
    turbineSize: number,
    durationSecond: number,
    k: number, // K x 2PI
    renderItem1?: any,
    renderItem2?: any,
    renderItem3?: any,
    renderItem4?: any,
    renderItem5?: any,
    renderItem6?: any,
}

interface State {
    duration: number,
    deg: string,
    rotatable: boolean,
    containerLayout: Layout,
    item1Layout: Layout,
    direction: Direction,
}

type Direction = "left" | "right";
type ItemName = "item1" | "item2" | "item3" | "item4" | "item5" | "item6";

interface Position {
    x: number,
    y: number,
}

interface Layout extends Position {
    width: number,
    height: number
}

interface CenterParams {
    x: number,
    y: number,
    width: number,
    height: number,
}

interface GetNextPosParams {
    x: number,
    y: number,
    degNumber: number,
    a: number,
    b: number,
}

interface GetItemAnimArrayParams {
    centerX: number,
    centerY: number,
    standardX: number,
    standardY: number,
    direction: Direction,
    deg: string,
}

const SECOND = 1000;
const TURBINE_SIZE = 200;
const ITEM_SIZE = TURBINE_SIZE / 4;

const defaultLayout: Layout = {
    x: 0,
    y: 0,
    height: 0,
    width: 0,
};

function parseToDegNumber(k: number) {
    if (k > 0) {
        return k * 360;
    }
    return 0;
}

function parseDegNumberToDeg(dpNumber: number) {
    if (dpNumber > 0 || dpNumber < 0) {
        return dpNumber + "deg";
    }
    return "0deg";
}

function parseDegToDegNumber(deg: string) {
    if (deg.length > 3) {
        const hasDp = _.endsWith(deg, "deg");
        if (hasDp) {
            const tempDeg: string | number = deg.slice(0, -3);
            if (tempDeg.length > 0) {
                const degNumber = parseInt(tempDeg);
                if (_.isNumber(degNumber)) {
                    return degNumber;
                }
            }
        }
    }
    return 0;
}


function getCenter(params: CenterParams) {
    if (params) {
        const {
            x,
            y,
            width,
            height,
        } = params;
        if (_.isNumber(x) && _.isNumber(y) && width > 0 && height > 0) {
            return {
                x: x + width / 2,
                y: y + height / 2,
            }
        }
    }
    return {
        x: 0,
        y: 0,
    };
}

function getRadianNumber(reg: number) {
    if (_.isNumber(reg)) {
        return Math.PI / 180 * reg;
    }
    return 0;
}

// x' = (x - a)*cos(deg) - (y - b)*sin(deg) + a
function getNextX(params: GetNextPosParams) {
    if (params) {
        const {
            a = 0,
            b = 0,
            degNumber = 0,
            x = 0,
            y = 0,
        } = params;
        const nextX = ((x - a) * Math.cos(getRadianNumber(degNumber))) - ((y - b) * Math.sin(getRadianNumber(degNumber))) + a;
        return nextX;
    }
    return 0;
}

// y' = (x - a)*sin(deg) + (y-b)* sin(deg) +b;
export function getNextY(params: GetNextPosParams) {
    if (params) {
        const {
            a = 0,
            b = 0,
            degNumber = 0,
            x = 0,
            y = 0,
        } = params;
        const nextY = ((x - a) * Math.sin(getRadianNumber(degNumber))) + ((y - b) * Math.cos(getRadianNumber(degNumber))) + b;
        return nextY;
    }
    return 0;
}

interface GetDParams {
    x1: number,
    x2: number,
    y1: number,
    y2: number,
}

function getD(params: GetDParams) {
    if (params) {
        const {
            x1 = 0,
            x2 = 0,
            y1 = 0,
            y2 = 0,
        } = params;
        return Math.sqrt(Math.pow(x2 - x1, 2) + (Math.pow(y2 - y1, 2)));
    }
    return 0;
}

export function checkPosition(degNumber: number): 1 | 2 | 3 | 4 {
    if (_.isNumber(degNumber)) {
        const residualNumber = degNumber % 360;
        if (residualNumber > 0 && residualNumber <= 90) {
            return 1;
        } else if (residualNumber > 90 && residualNumber <= 180) {
            return 2;
        } else if (residualNumber > 180 && residualNumber <= 270) {
            return 3;
        }
        return 4;
    }
    return 1;
}

const EACH_ITEM_ANGEL = 60;
const ITEM_1_ANGLE = 0;
const ITEM_2_ANGLE = EACH_ITEM_ANGEL;
const ITEM_3_ANGLE = EACH_ITEM_ANGEL * 2;
const ITEM_4_ANGLE = EACH_ITEM_ANGEL * 3;
const ITEM_5_ANGLE = EACH_ITEM_ANGEL * 4;
const ITEM_6_ANGLE = EACH_ITEM_ANGEL * 5;

const turbineCenter = {
    x: TURBINE_SIZE / 2,
    y: TURBINE_SIZE / 2,
};

const standardItemCenter = {
    x: TURBINE_SIZE / 2,
    y: ITEM_SIZE / 2,
};

const initCenter = {
    item1: standardItemCenter,
    item2: {
        x: getNextX({
            a: turbineCenter.x,
            b: turbineCenter.y,
            x: standardItemCenter.x,
            y: standardItemCenter.y,
            degNumber: ITEM_2_ANGLE
        }),
        y: getNextY({
            a: turbineCenter.x,
            b: turbineCenter.y,
            x: standardItemCenter.x,
            y: standardItemCenter.y,
            degNumber: ITEM_2_ANGLE
        }),
    },
    item3: {
        x: getNextX({
            a: turbineCenter.x,
            b: turbineCenter.y,
            x: standardItemCenter.x,
            y: standardItemCenter.y,
            degNumber: ITEM_3_ANGLE
        }),
        y: getNextY({
            a: turbineCenter.x,
            b: turbineCenter.y,
            x: standardItemCenter.x,
            y: standardItemCenter.y,
            degNumber: ITEM_3_ANGLE
        }),
    },
    item4: {
        x: getNextX({
            a: turbineCenter.x,
            b: turbineCenter.y,
            x: standardItemCenter.x,
            y: standardItemCenter.y,
            degNumber: ITEM_4_ANGLE
        }),
        y: getNextY({
            a: turbineCenter.x,
            b: turbineCenter.y,
            x: standardItemCenter.x,
            y: standardItemCenter.y,
            degNumber: ITEM_4_ANGLE
        }),
    },
    item5: {
        x: getNextX({
            a: turbineCenter.x,
            b: turbineCenter.y,
            x: standardItemCenter.x,
            y: standardItemCenter.y,
            degNumber: ITEM_5_ANGLE
        }),
        y: getNextY({
            a: turbineCenter.x,
            b: turbineCenter.y,
            x: standardItemCenter.x,
            y: standardItemCenter.y,
            degNumber: ITEM_5_ANGLE
        }),
    },
    item6: {
        x: getNextX({
            a: turbineCenter.x,
            b: turbineCenter.y,
            x: standardItemCenter.x,
            y: standardItemCenter.y,
            degNumber: ITEM_6_ANGLE
        }),
        y: getNextY({
            a: turbineCenter.x,
            b: turbineCenter.y,
            x: standardItemCenter.x,
            y: standardItemCenter.y,
            degNumber: ITEM_6_ANGLE
        }),
    },
};

const initPosition = {
    item1: {
        x: initCenter.item1.x - ITEM_SIZE / 2,
        y: 0,
    },
    item2: {
        x: initCenter.item2.x - ITEM_SIZE / 2,
        y: initCenter.item2.y - ITEM_SIZE / 2,
    },
    item3: {
        x: initCenter.item3.x - ITEM_SIZE / 2,
        y: initCenter.item3.y - ITEM_SIZE / 2,
    },
    item4: {
        x: initCenter.item4.x - ITEM_SIZE / 2,
        y: initCenter.item4.y - ITEM_SIZE / 2,
    },
    item5: {
        x: initCenter.item5.x - ITEM_SIZE / 2,
        y: initCenter.item5.y - ITEM_SIZE / 2,
    },
    item6: {
        x: initCenter.item6.x - ITEM_SIZE / 2,
        y: initCenter.item6.y - ITEM_SIZE / 2,
    },
};

export default class Turbine extends PureComponent<Props, State> {
    static defaultProps: Props | any = {
        turbineSize: TURBINE_SIZE,
        durationSecond: 2,
        k: 1,
    };

    animatedValue = new Animated.Value(0);

    memoItem2Center = memoize((centerX = 0, centerY = 0, initX = 0, initY = 0): Position => {
        const props = {a: centerX, b: centerY, x: initX, y: initY, degNumber: 60};
        return {
            x: getNextX(props),
            y: getNextY(props),
        };
    });

    memoItem3Center = memoize((centerX = 0, centerY = 0, initX = 0, initY = 0): Position => {
        const props = {a: centerX, b: centerY, x: initX, y: initY, degNumber: 120};
        return {
            x: getNextX(props),
            y: getNextY(props),
        };
    });

    memoItem4Center = memoize((centerX = 0, centerY = 0, initX = 0, initY = 0): Position => {
        const props = {a: centerX, b: centerY, x: initX, y: initY, degNumber: 180};
        return {
            x: getNextX(props),
            y: getNextY(props),
        };
    });

    memoItem5Center = memoize((centerX = 0, centerY = 0, initX = 0, initY = 0): Position => {
        const props = {a: centerX, b: centerY, x: initX, y: initY, degNumber: 240};
        return {
            x: getNextX(props),
            y: getNextY(props),
        };
    });

    memoItem6Center = memoize((centerX = 0, centerY = 0, initX = 0, initY = 0): Position => {
        const props = {a: centerX, b: centerY, x: initX, y: initY, degNumber: 300};
        return {
            x: getNextX(props),
            y: getNextY(props),
        };
    });

    memoItemAnimLeftArray = memoize((centerX = 0, centerY = 0, standardX = 0, standardY = 0, deg: string) => {
        return this.getItemAnimArray({
            centerX,
            centerY,
            standardX,
            standardY,
            deg,
            direction: "left",
        });
    });

    memoItemAnimRightArray = memoize((centerX = 0, centerY = 0, standardX = 0, standardY = 0, deg: string) => {
        return this.getItemAnimArray({
            centerX,
            centerY,
            standardX,
            standardY,
            deg,
            direction: "right",
        });
    });

    getItemAnimArray = (params: GetItemAnimArrayParams) => {
        if (params) {
            const {
                centerX,
                centerY,
                standardX,
                standardY,
                direction,
                deg,
            } = params;
            const degNumberArray: any[] = [];

            const item1TranslateXArray: any[] = [];
            const item1TranslateYArray: any[] = [];

            const item2TranslateXArray: any[] = [];
            const item2TranslateYArray: any[] = [];

            const item3TranslateXArray: any[] = [];
            const item3TranslateYArray: any[] = [];

            const item4TranslateXArray: any[] = [];
            const item4TranslateYArray: any[] = [];

            const item5TranslateXArray: any[] = [];
            const item5TranslateYArray: any[] = [];

            const item6TranslateXArray: any[] = [];
            const item6TranslateYArray: any[] = [];
            const lastPos = {
                item1: {
                    x: initCenter.item1.x,
                    y: initCenter.item1.y,
                },
                item2: {
                    x: initCenter.item2.x,
                    y: initCenter.item2.y,
                },
                item3: {
                    x: initCenter.item3.x,
                    y: initCenter.item3.y,
                },
                item4: {
                    x: initCenter.item4.x,
                    y: initCenter.item4.y,
                },
                item5: {
                    x: initCenter.item5.x,
                    y: initCenter.item5.y,
                },
                item6: {
                    x: initCenter.item6.x,
                    y: initCenter.item6.y,
                },
            };
            const lastTrans = {
                item1: {
                    x: 0,
                    y: 0,
                },
                item2: {
                    x: 0,
                    y: 0,
                },
                item3: {
                    x: 0,
                    y: 0,
                },
                item4: {
                    x: 0,
                    y: 0,
                },
                item5: {
                    x: 0,
                    y: 0,
                },
                item6: {
                    x: 0,
                    y: 0,
                }
            };

            function getCommonNextX(degNumber = 0) {
                return getNextX({
                    a: turbineCenter.x,
                    b: turbineCenter.y,
                    x: initCenter.item1.x,
                    y: initCenter.item1.y,
                    degNumber: degNumber,
                });
            }

            function getCommonNextY(degNumber = 0) {
                return getNextY({
                    a: turbineCenter.x,
                    b: turbineCenter.y,
                    x: initCenter.item1.x,
                    y: initCenter.item1.y,
                    degNumber: degNumber,
                });
            }

            for (let i = 0; direction === "left" ? i >= -parseDegToDegNumber(deg) : i <= parseDegToDegNumber(deg); direction === "left" ? i -= 20 : i += 20) {
                degNumberArray.push(i);
                const currentPos = {
                    item1: {
                        x: getCommonNextX(i),
                        y: getCommonNextY(i),
                    },
                    item2: {
                        x: getCommonNextX(ITEM_2_ANGLE + i),
                        y: getCommonNextY(ITEM_2_ANGLE + i),
                    },

                    item3: {
                        x: getCommonNextX(ITEM_3_ANGLE + i),
                        y: getCommonNextY(ITEM_3_ANGLE + i),
                    },
                    item4: {
                        x: getCommonNextX(ITEM_4_ANGLE + i),
                        y: getCommonNextY(ITEM_4_ANGLE + i),
                    },
                    item5: {
                        x: getCommonNextX(ITEM_5_ANGLE + i),
                        y: getCommonNextY(ITEM_5_ANGLE + i),
                    },
                    item6: {
                        x: getCommonNextX(ITEM_6_ANGLE + i),
                        y: getCommonNextY(ITEM_6_ANGLE + i),
                    },
                };

                function getDelta(name: ItemName) {
                    return {
                        x: currentPos[name].x - lastPos[name].x,
                        y: currentPos[name].y - lastPos[name].y,
                    };
                }

                const delta = {
                    item1: getDelta("item1"),
                    item2: getDelta("item2"),
                    item3: getDelta("item3"),
                    item4: getDelta("item4"),
                    item5: getDelta("item5"),
                    item6: getDelta("item6"),
                };

                // console.log("delta:", delta.item1);

                function getTrans(name: ItemName) {
                    return {
                        x: delta[name].x + lastTrans[name].x,
                        y: delta[name].y + lastTrans[name].y,
                    };
                }

                const trans = {
                    item1: getTrans("item1"),
                    item2: getTrans("item2"),
                    item3: getTrans("item3"),
                    item4: getTrans("item4"),
                    item5: getTrans("item5"),
                    item6: getTrans("item6"),
                };

                item1TranslateXArray.push(trans.item1.x);
                item1TranslateYArray.push(trans.item1.y);

                item2TranslateXArray.push(trans.item2.x);
                item2TranslateYArray.push(trans.item2.y);

                item3TranslateXArray.push(trans.item3.x);
                item3TranslateYArray.push(trans.item3.y);

                item4TranslateXArray.push(trans.item4.x);
                item4TranslateYArray.push(trans.item4.y);

                item5TranslateXArray.push(trans.item5.x);
                item5TranslateYArray.push(trans.item5.y);

                item6TranslateXArray.push(trans.item6.x);
                item6TranslateYArray.push(trans.item6.y);

                function updateProps(name: ItemName): void {
                    lastPos[name].x = currentPos[name].x;
                    lastPos[name].y = currentPos[name].y;
                    lastTrans[name].x = trans[name].x;
                    lastTrans[name].y = trans[name].y;
                }

                // update last props
                updateProps("item1");
                updateProps("item2");
                updateProps("item3");
                updateProps("item4");
                updateProps("item5");
                updateProps("item6");
            }

            const revered1X = [...item1TranslateXArray].reverse();
            const revered2X = [...item2TranslateXArray].reverse();
            const revered3X = [...item3TranslateXArray].reverse();
            const revered4X = [...item4TranslateXArray].reverse();
            const revered5X = [...item5TranslateXArray].reverse();
            const revered6X = [...item6TranslateXArray].reverse();

            const revered1Y = [...item1TranslateYArray].reverse();
            const revered2Y = [...item2TranslateYArray].reverse();
            const revered3Y = [...item3TranslateYArray].reverse();
            const revered4Y = [...item4TranslateYArray].reverse();
            const revered5Y = [...item5TranslateYArray].reverse();
            const revered6Y = [...item6TranslateYArray].reverse();

            const revertedDegNumberArray = [...degNumberArray].reverse();
            const left = direction === "left";
            return {
                item1: {
                    x: left ? revered1X : item1TranslateXArray,
                    y: left ? revered1Y : item1TranslateYArray,
                },
                item2: {
                    x: left ? revered2X : item2TranslateXArray,
                    y: left ? revered2Y : item2TranslateYArray,
                },
                item3: {
                    x: left ? revered3X : item3TranslateXArray,
                    y: left ? revered3Y : item3TranslateYArray,
                },
                item4: {
                    x: left ? revered4X : item4TranslateXArray,
                    y: left ? revered4Y : item4TranslateYArray,
                },
                item5: {
                    x: left ? revered5X : item5TranslateXArray,
                    y: left ? revered5Y : item5TranslateYArray,
                },
                item6: {
                    x: left ? revered6X : item6TranslateXArray,
                    y: left ? revered6Y : item6TranslateYArray,
                },
                degNumberArray: left ? revertedDegNumberArray : degNumberArray,
            };
        }
    };

    state: State = {
        deg: parseDegNumberToDeg(0),
        duration: 0,
        containerLayout: defaultLayout,
        rotatable: true,
        item1Layout: defaultLayout,
        direction: "right",
    };


    constructor(props: Props) {
        super(props);
        const {
            durationSecond,
            k,
        } = this.props;
        this.state = {
            duration: durationSecond * SECOND,
            deg: parseDegNumberToDeg(parseToDegNumber(k)),
            containerLayout: this.state.containerLayout,
            rotatable: this.state.rotatable,
            item1Layout: this.state.item1Layout,
            direction: this.state.direction,
        };
    }

    render() {
        const {
            deg,
            containerLayout,
            item1Layout,
            direction,
        } = this.state;

        const center = getCenter(containerLayout);
        const a = center.x;
        const b = center.y;
        const item1Center = getCenter(item1Layout);
        const item2Center = this.memoItem2Center(a, b, item1Center.x, item1Center.y);

        const turbineRotateAnim = this.animatedValue.interpolate({
            inputRange: [-parseDegToDegNumber(deg), 0, parseDegToDegNumber(deg)],
            outputRange: ["-" + deg, parseDegNumberToDeg(0), deg],
        });
        const revertedTurbineRotateAnim = this.animatedValue.interpolate({
            inputRange: [-parseDegToDegNumber(deg), 0, parseDegToDegNumber(deg)],
            outputRange: [deg, parseDegNumberToDeg(0), "-" + deg],
        });
        const containerOpacityAnim = this.animatedValue.interpolate({
            inputRange: [-parseDegToDegNumber(deg), 0, parseDegToDegNumber(deg)],
            outputRange: [1, 1, 1],
        });
        const containerScaleAnim = this.animatedValue.interpolate({
            inputRange: [-parseDegToDegNumber(deg), 0, parseDegToDegNumber(deg) / 4, parseDegToDegNumber(deg)],
            outputRange: [1, 1, 0.6, 0.5],
        });
        const containerTranslateX = this.animatedValue.interpolate({
            inputRange: [-parseDegToDegNumber(deg), 0, parseDegToDegNumber(deg) / 2, parseDegToDegNumber(deg)],
            outputRange: [TURBINE_SIZE, 0, TURBINE_SIZE, 2 * TURBINE_SIZE],
        });

        let item1XAnim: any;
        let item1YAnim: any;
        let item2XAnim: any;
        let item2YAnim: any;
        let item3XAnim: any;
        let item3YAnim: any;
        let item4XAnim: any;
        let item4YAnim: any;
        let item5XAnim: any;
        let item5YAnim: any;
        let item6XAnim: any;
        let item6YAnim: any;

        if (direction === "left") {
            const object = this.memoItemAnimLeftArray(turbineCenter.x, turbineCenter.y, standardItemCenter.x, standardItemCenter.y, deg);
            if (object) {
                const {
                    degNumberArray,
                } = object;
                const getAnimX = (name: ItemName) => {
                    return this.animatedValue.interpolate({
                        inputRange: degNumberArray,
                        outputRange: object[name].x,
                    });
                };
                const getAnimY = (name: ItemName) => {
                    return this.animatedValue.interpolate({
                        inputRange: degNumberArray,
                        outputRange: object[name].y,
                    });
                };
                item1XAnim = getAnimX("item1");
                item1YAnim = getAnimY("item1");

                item2XAnim = getAnimX("item2");
                item2YAnim = getAnimY("item2");

                item3XAnim = getAnimX("item3");
                item3YAnim = getAnimY("item3");

                item4XAnim = getAnimX("item4");
                item4YAnim = getAnimY("item4");

                item5XAnim = getAnimX("item5");
                item5YAnim = getAnimY("item5");

                item6XAnim = getAnimX("item6");
                item6YAnim = getAnimY("item6");
            }
        } else {
            const object = this.memoItemAnimRightArray(turbineCenter.x, turbineCenter.y, standardItemCenter.x, standardItemCenter.y, deg);
            if (object) {
                const {
                    degNumberArray,
                } = object;
                const getAnimX = (name: ItemName) => {
                    return this.animatedValue.interpolate({
                        inputRange: degNumberArray,
                        outputRange: object[name].x,
                    });
                };
                const getAnimY = (name: ItemName) => {
                    return this.animatedValue.interpolate({
                        inputRange: degNumberArray,
                        outputRange: object[name].y,
                    });
                };
                item1XAnim = getAnimX("item1");
                item1YAnim = getAnimY("item1");

                item2XAnim = getAnimX("item2");
                item2YAnim = getAnimY("item2");

                item3XAnim = getAnimX("item3");
                item3YAnim = getAnimY("item3");

                item4XAnim = getAnimX("item4");
                item4YAnim = getAnimY("item4");

                item5XAnim = getAnimX("item5");
                item5YAnim = getAnimY("item5");

                item6XAnim = getAnimX("item6");
                item6YAnim = getAnimY("item6");
            }
        }

        return (
            <Animated.View style={[styles.container, {
                transform: [
                    {scaleX: containerScaleAnim},
                    {scaleY: containerScaleAnim},
                    {translateX: containerTranslateX}
                ],
                opacity: containerOpacityAnim
            }]}>
                <Animated.View style={[styles.turbine, styles.absolute, {
                    transform: [{rotate: revertedTurbineRotateAnim}],
                    backgroundColor: "green",
                }]}>

                </Animated.View>
                <Animated.View style={[styles.turbine, {
                    transform: [{rotate: turbineRotateAnim}],
                }]} onLayout={this.onLayout}>

                </Animated.View>
                {this.renderItem("item1", item1XAnim, item1YAnim)}
                {this.renderItem("item2", item2XAnim, item2YAnim)}
                {this.renderItem("item3", item3XAnim, item3YAnim)}
                {this.renderItem("item4", item4XAnim, item4YAnim)}
                {this.renderItem("item5", item5XAnim, item5YAnim)}
                {this.renderItem("item6", item6XAnim, item6YAnim)}
                <TouchableOpacity style={[styles.absolute, styles.flexCenterAll]} onPress={this.onRotateRight}>
                    <Text>
                        {"Rotate"}
                    </Text>
                </TouchableOpacity>
            </Animated.View>
        );
    }

    onLayout = (event: any) => {
        const {
            height,
            width,
            x,
            y,
        } = event.nativeEvent.layout;
        this.setLayout(event.nativeEvent.layout, "containerLayout");
    };

    onRotateRight = () => {
        this.onRotate("right");
    };

    onRotateLeft = () => {
        this.onRotate("left");
    };

    onRotate = (direction: Direction) => {
        const {
            duration,
            deg,
            rotatable,
        } = this.state;
        if (rotatable) {
            this.setState({
                rotatable: false,
                direction: direction,
            }, () => {
                Animated.timing(this.animatedValue, {
                    duration,
                    toValue: parseDegToDegNumber(deg) * (direction === "left" ? -1 : 1),
                }).start(({finished}) => {
                    if (finished) {
                        this.resetAnimation();
                    }
                });
            });
        }
    };

    onChildLayout = (event: any) => {
        const layout = event.nativeEvent.layout;
        this.setLayout(layout, "item1Layout")
    };

    resetAnimation = () => {
        this.setState({
            rotatable: true,
        }, () => {
            this.animatedValue.setValue(0);
        });
    };

    setLayout = (layout: Layout, layoutName: "containerLayout" | "item1Layout" = "containerLayout") => {
        if (layoutName && layout) {
            const {
                x,
                y,
                width,
                height,
            } = layout;
            const currentLayout = this.state[layoutName];
            let currentX = 0;
            let currentY = 0;
            let currentWidth = 0;
            let currentHeight = 0;
            if (currentLayout) {
                currentX = currentLayout.x;
                currentY = currentLayout.y;
                currentWidth = currentLayout.width;
                currentHeight = currentLayout.height;
            }
            if (x !== currentX || y !== currentY || width !== currentWidth || height !== currentHeight) {
                //@ts-ignore
                this.setState({
                    [layoutName]: {
                        x,
                        y,
                        width,
                        height,
                    }
                });
            }
        }
    };

    renderItem = (name: ItemName, animValueX: any = 0, animValueY: any = 0) => {
        const {
            renderItem1,
            renderItem2,
            renderItem3,
            renderItem4,
            renderItem5,
            renderItem6,
        } = this.props;
        let item: any;
        switch (name) {
            case "item1":
                item = renderItem1 && renderItem1();
                break;
            case "item2":
                item = renderItem2 && renderItem2();
                break;
            case "item3":
                item = renderItem3 && renderItem3();
                break;
            case "item4":
                item = renderItem4 && renderItem4();
                break;
            case "item5":
                item = renderItem5 && renderItem5();
                break;
            case "item6":
                item = renderItem6 && renderItem6();
                break;
        }
        return (
            <Animated.View
                style={{
                    position: "absolute",
                    left: initPosition[name].x,
                    top: initPosition[name].y,
                    width: ITEM_SIZE,
                    height: ITEM_SIZE,
                    backgroundColor: "#f5f5f5",
                    borderRadius: ITEM_SIZE / 4,
                    transform: [{translateX: animValueX}, {translateY: animValueY}]
                }}
            >
                {item}
            </Animated.View>
        );
    };

}

const styles = StyleSheet.create({
    container: {},
    flexCenterAll: {
        justifyContent: "center",
        alignItems: "center",
    },
    absolute: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    turbine: {
        height: TURBINE_SIZE,
        width: TURBINE_SIZE,
        backgroundColor: "#487eff",
        justifyContent: "flex-start",
        alignItems: "center",
    },
});
