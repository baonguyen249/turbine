import React, {PureComponent} from "react";
import {StyleSheet, Text, View} from "react-native";
import Turbine, {checkPosition} from "./src/gui/components/Turbine";

interface Props {

}

interface State {

}

export default class App extends PureComponent<Props, State> {
    render() {

        return (
            <View style={[styles.container]} onLayout={this.onLayout}>
                <Turbine renderItem1={this.renderItem1}/>
            </View>
        );
    }

    onLayout = (event: any) => {
        // console.log("event App:", event.nativeEvent);
    };

    renderItem1 = () => {
        return (
            <View style={{flex: 1, backgroundColor: "red"}}>
                <Text>
                    {"item 1"}
                </Text>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
});


